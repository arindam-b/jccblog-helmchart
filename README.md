# Deployed in AWS EKS Cluster

# Deployment
helm upgrade --install jccblog-chart . --values values-prod.yaml

# Delete
helm uninstall jccblog-chart .

# To find deployed helm charts

helm history jccblog-chart

# Rollback to a specific version

helm rollback jccblog-chart 1

# Please note, there is no configmap and secret for the environment so far which is unlikely in the real world application. 


# This deployment is taken into account to the context of AWS EKS:

Security group of the ALB Ingress controller - assumed to sg-XXXXX

Service-account to IAM Binding to be assumed, if any in case of case to case basis.

# Assumed, metrics server is already deployed, hence HPA is enabled in this helm chart.



